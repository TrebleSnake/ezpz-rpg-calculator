$(document).ready(function () {

    var i18n = I18N();

    function calculate() {
        var value = $('#currentValue').val();
        var grade = $('#currentGrade').val();

        Calculator($('#resultsContainer'), value, grade, i18n).printResults()
    }

    $('#doTheThing').click(function () {
        calculate()
    });

    $('#inputForm').submit(function(event){
        calculate()
        event.preventDefault();
    });

    i18n.init();
});



function I18N() {
    return {
        LANGS: ['ru', 'en'],
        DEFAULT_LANG: 'en',
        PROPERTIES: {
            'ru': {
                'base': 'Базовый'
            },
            'en': {
                'base': 'Base'
            }
        },

        setLang: function (lang) {
            if (this.getLang() == lang || !_.contains(this.LANGS, lang))
                return;

            $.cookie('lang', lang);
            $('.i18n-control.label-info').removeClass('label-info').addClass('label-default');
            $('.i18n-control[rel=' + lang + ']').removeClass('label-default').addClass('label-info');
            $('.i18n').hide()
                .filter('.' + lang).show();
        },

        getLang: function () {
            return $('.i18n-control.label-info').attr('rel');
        },

        init: function () {
            var self = this;
            _.each(this.LANGS, function(item) {
                $('#i18n-controls').append('<span class="i18n-control label label-default" rel="'+item+'">'+item+'</span> ')
            });

            $('.i18n-control').click(function () {
                self.setLang($(this).attr('rel'))
            });

            var lang = $.cookie('lang');
            this.setLang(_.isUndefined(lang) || !_.contains(this.LANGS, lang) ? this.DEFAULT_LANG : lang);
        },

        get: function(property) {
            var value = this.PROPERTIES[this.getLang()][property];
            return _.isUndefined(value) ? '-' : value;
        }
    }
}

function Calculator(parent, initialValue, initialGrade, i18n) {
    return {
        ROW_TPL: '<div class="row"></div>',
        CELL_TPL: '<div class="cell col-xs-2{ACTIVE}"><div class="grade">{GRADE}</div><div class="value">{VALUE}</div></div>',
        DUMMY_TPL: '<div class="col-xs-1"></div>',
        BASE_CELL_TPL: '<div class="cell base col-xs-10{ACTIVE}"><div class="grade">{GRADE}</div><div class="value">{VALUE}</div></div>',
        RATIO: [1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.65, 1.8, 1.95, 2.1, 2.25, 2.4, 2.55, 2.7, 2.85, 3],
        ITEMS_PER_LINE: 5,

        printResults: function () {
            var grades = this._calculateGrades(initialValue, initialGrade);
            var gradesQty = grades.length;

            // clear
            parent.empty();

            // append base
            var row = this._row();
            row.append(this._dummy());
            row = this._createRow(row,
                this._cell(grades[0], i18n.get('base'), initialGrade == 0, this.BASE_CELL_TPL), true);

            for (var i = 1; i < gradesQty; i++) {
                row = this._createRow(row,
                    this._cell(grades[i], '+' + i, initialGrade == i), i % this.ITEMS_PER_LINE == 0);
            }
            parent.append(row);
        },

        _createRow: function (row, cell, nextRow) {
            row.append(cell);

            if (nextRow) {
                row.append(this._dummy());
                parent.append(row);
                row = this._row();
                row.append(this._dummy());
            }
            return row
        },

        _row: function () {
            return $(this.ROW_TPL)
        },

        _cell: function (value, grade, active, tpl) {
            var tpl = _.isUndefined(tpl) ? this.CELL_TPL : tpl;
            var active = active ? ' active' : '';
            return $(tpl
                    .replace('{VALUE}', value)
                    .replace('{GRADE}', grade)
                    .replace('{ACTIVE}', active)
            )
        },

        _dummy: function () {
          return $(this.DUMMY_TPL)
        },

        _calculateGrades: function (value, grade) {
            var base = value / this.RATIO[grade];
            var gradesQty = this.RATIO.length;
            var result = [];
            for (var i = 0; i < gradesQty; i++) {
                result.push(Math.round(base * this.RATIO[i]));
            }
            return result;
        }
    }
}